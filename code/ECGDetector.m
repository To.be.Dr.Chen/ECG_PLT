    classdef ECGDetector < PeakDetector
    %ECGDETECTOR ECG Peak Detector
    %   
    %   Realtime ECG Peak Detector
    %   
    %   Participants of the BMT Lab at IBT have to modify 
    %   this class in exercise 6.
    %
    
    properties
        
        %% YOU CAN ADD OWN PROPERTIES HERE
        
        
        
    end
    
    
    methods        
        
        function peak = process_single(mf, time_in, sig_in)
            %% This is your primary function for the peakdetection
            %  
            %   Input: 
            %           mf    :     Instance of your Peakdetector (
            %           ECG/Pleth)
            %
            %           time_in, sig_in : contains a (filtered) signalvalue
            %           and its time
            %
            %
            %   Output:
            %           peak   :    time at which a peak occurs. Consider:
            %           can be empty, you dont need to return a peak the
            %           moment it occurs...
            %           
            %
            %% Check inputs
            assert(isscalar(sig_in))
            assert(isa(sig_in,'double'))
            assert(isreal(sig_in))
            
            peak = [];
            
            %% ADD YOUR CODE HERE
            
            mf.Preg=[mf.Preg sig_in];
            mf.Treg=[mf.Treg time_in];
            lgx=length(mf.Preg);
            if (lgx==1500)
            Averg=mean(mf.Preg); %% average
            Abweic= std(mf.Preg); %%standard deviation
            Schw=Averg+2*Abweic; %%schwelle
%             B=[mf.Preg(1:1400)-mf.Preg(101:1500)];
%             Schwslp=max(B)*2/3;
             [Pks, Loc]=findpeaks(mf.Preg, 'MinPeakDistance', 250);
%               [Pks, Loc]=findpeaks(mf.Preg);
             for idx=(1:length(Pks))
                if(Pks(idx)>Schw)
% %                     if(idx<=100)
% %                         peak=peak;
% %                     else
% %                     if((mf.Preg(idx)-mf.Preg(idx-100))>0&&(mf.Preg(idx)-mf.Preg(idx+100))>0)
                   peak=[peak mf.Treg(Loc(idx))];
% %                     end
%                     end
                end
             end
             
             mf.Preg=[];
            mf.Treg=[];
            end
            
%             if(lgx<=2)
%                 peak = [];
%             end
%             if (lgx>2)
%                 if(mf.Preg(lgx-1)>mf.Preg(lgx-2)&&mf.Preg(lgx-1)>mf.Preg(lgx)&&mf.Preg(lgx-1)>Schw)
%                    peak=mf.Treg(lgx-1);
%                 end
%             end
            
           
        end
        
    end
    
end