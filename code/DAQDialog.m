function DAQDialog(callback)
%DAQDialog Presents a GUI to create a configured DAQ session for NI USB-6008/6009 
% DAQDialog is just a simple tool that provides the DAQ parameters I need
% in a specific project with biosignal. 
% For broader usage, it needs to be refactored. 
% Remembers the settings between program invocations using setpref/getpref.
% 
% 
% Input argument
%   - Callback function that will be called after the user presses OK. 
%     It is called with one argument, "result". 
%     The created session is "result.session". 
%
% (No output arguments. Using the callback we avoid uiwait.)
%
% TODO 
%   - Apply changes immediately using CellEditFunction of uitable, 
%     like "Data Acquisition Toolbox" from Matlab Central File Exchange. 
%     (Unfortunately it is p-coded so we can't just use it)
%
%
% Version 1.2 - 27.02.2017 - Daniel Frisch 
%
%
%


%% Dependencies
% [flist,plist] = matlab.codetools.requiredFilesAndProducts('DAQDialog.m'); [flist'; {plist.Name}']
%
% MATLAB; Data Acquisition Toolbox
%


%% Allow only one dialog

persistent fig

if ~isempty(fig) && isvalid(fig)
    figure(fig)
    return
end


%% Preferences

% Validation Functions
charChk   = @(x) validateattributes(x,{'char'           },{'vector'});
rateChk   = @(x) validateattributes(x,{'double','single'},{'real','nonnan','finite','positive','scalar'});
posiChk   = @(x) validateattributes(x,{'double'},{'real','nonnan','finite','positive','size',[1,4]});
boolChk   = @(x) validateattributes(x,{'logical'},{'scalar'});
stctChk   = @(x) validateattributes(x,{'struct'});
cellChk   = @(x) validateattributes(x,{'cell'},{});

% Default Preferences and Validation
pref_default = struct();
pref_default.OuterPosition                        = prefentry([1133 551 576 512],posiChk);
pref_default.DeviceID                             = prefentry('Dev1',charChk);
pref_default.Rate                                 = prefentry(1000,rateChk);
pref_default.IsContinuous                         = prefentry(true,boolChk);
pref_default.DurationInSeconds                    = prefentry(10,rateChk);
pref_default.IsNotifyWhenDataAvailableExceedsAuto = prefentry(true,boolChk);
pref_default.NotifyWhenDataAvailableExceeds       = prefentry(100,rateChk);
pref_default.ChannelInfo_Input                    = prefentry({},cellChk);


% Update pref table with valid preferences from registry (getpref). 
prefgp = 'com_gmail_danielfrischkit_DAQDialog';
pref = pref_default;
propnames = fieldnames(pref);
nProp = length(propnames);

for iProp = 1:nProp
    propname = propnames{iProp};
    if ispref(prefgp, propname)
        val = getpref(prefgp, propname);
        func = pref.(propname).Validation;
        try
            feval(func,val);
            pref.(propname).Value = val;
        catch ex
            str = ex.message;
            disp(str)
            if ~isempty(str)
                fprintf('%s \n',str)
            end
        end
    end
end



%% Definitions

% Range
view = {'�20 V','�10 V','�5 V','�4 V','�2.5 V','�2 V','�1.25 V','�1 V'}'; 
num = {[-20,20], [-10,10], [-5,5], [-4,4], [-2.5,2.5], [-2,2], [-1.25,1.25], [-1,1]}';
range_table = table(view,num);

n2s = @(x) num2str(x,40);



%% GUI Initialization

fig = figure();

device_list = uicontrol(...
    'Style','popupmenu',...
    'String',' ',...
    'Parent',fig);

devprop_table = uitable('Parent',fig);

devprop_help = uicontrol('Style','pushbutton', 'Parent',fig);

channel_table = uitable('Parent',fig);

ok_button = uicontrol('Style','pushbutton', 'Parent',fig);




% GUI Configuration

set(fig,...
    'NumberTitle','off',...
    'Name','Please configure DAQ.',...
    'MenuBar','none',...
    'ToolBar','none',...
    'DockControls','off',...
    'OuterPosition',pref.OuterPosition.Value);

%fig.WindowStyle = 'modal';

set(device_list,...
    'String','Scanning Devices...',...
    'Units','normalized',...
    'Position',[0.1 .9 0.8 .06]);

    function help_callback(varargin)
        % TODO use selected cell
        %doc('daq/NotifyWhenDataAvailableExceeds')
        web(fullfile(matlabroot,'help','daq','propertylist.html'))
    end

set(devprop_table,...
    'ColumnName', {'Property','Value','Units'},...
    'ColumnFormat', {'char', 'char', 'char'},...
    'ColumnEditable', [false, true, false],...
    'ColumnWidth', {200, 100, 100},...
    'RowName',[],...
    'Units','normalized',...
    'Position',[0.1 .53 0.7 .35]);
set(devprop_help,...
    'String','?',...
    'Callback',@help_callback,...
    'Units','normalized',...
    'Position',[0.81 .53 0.09 .35]);

set(channel_table,...
    'ColumnName', {'Enabled','Range','Name'},...
    'ColumnFormat', {'logical', range_table.view', 'char'},...
    'ColumnEditable', [true, true, true],...
    'ColumnWidth', {60, 60, 60},...
    'RowName',{'ai0', 'ai1', 'ai2', 'ai3'},...
    'Units','normalized',...
    'Position',[0.1 .18 0.8 .31]);

set(ok_button, ...
    'String','Apply', ...
    'Callback',@ok_callback,...
    'Enable','off',...
    'Units','normalized',...
    'Position',[0.1 .045 0.8 0.11]);

movegui(fig)



%% Get DAQ Device Information


% Get Information about Devices (takes some seconds)
drawnow
daqreset
devices = daq.getDevices;

devprop_table.Data = {
    'Rate',n2s(pref.Rate.Value),'Hz';
    'IsContinuous',n2s(pref.IsContinuous.Value),'boolean';
    'DurationInSeconds',n2s(pref.DurationInSeconds.Value),'s';
    'NotifyWhenDataAvailableExceeds',n2s(pref.NotifyWhenDataAvailableExceeds.Value),'Samples';
    'IsNotifyWhenDataAvailableExceedsAuto', n2s(pref.IsNotifyWhenDataAvailableExceedsAuto.Value), 'boolean';
    };

ids = get(devices,'ID');
devices_str = strcat(ids, {': '}, get(devices,'Description'));
device_list.String = devices_str;
ind = find(ismember(ids,pref.DeviceID.Value));
if ~isscalar(ind) || ind<=0 || ind>length(devices_str)
    ind = 1;
end
device_list.Value = ind;

data = pref.ChannelInfo_Input.Value;
if isempty(data)
    firstline = {true, '�2 V', 'ECG I'};
    defaultline = {false, '�2 V', 'ECG II'};
    lines = 4;
    data = [firstline; repmat(defaultline,[lines-1,1])];
end
channel_table.Data = data;

ok_button.Enable = 'on';


    function ok_callback(varargin)
        
        % Prevent deleting figure while reading data
        %fig0 = fig;
        %function enabler(fig)
        %    set(fig, 'CloseRequestFcn','closereq')
        %end
        %cleaner = onCleanup(@() enabler(fig0));
        %fig.CloseRequestFcn = @(varargin) [];
        
        % Create Session & Set Session Properties
        properties = devprop_table.Data(:,1);
        values = devprop_table.Data(:,2);
        
        if isempty(devices) % no device available
            disp(devices)
            return;
        end
        
        device = devices(device_list.Value);
        pref.DeviceID.Value = device.ID;
        
        % session = daq.createSession('ni')
        session = daq.createSession(device.Vendor.ID);
        
        for iPr = 1:length(properties)
            prop = properties{iPr};
            value = values{iPr};
            
            % Check type
            current = get(session,prop);
            switch class(current)
                case {'double','uint64'}
                    value = str2double(value);
                case 'logical'
                    switch value
                        case {'1','true','on','ja'}
                            value = true;
                        case {'0','false','off','nein'}
                            value = false;
                        otherwise
                            error('Unknown logical string: %s',value)
                    end
                otherwise
                    error('Unknown target class: %s',class(current))
            end
            pref.(prop).Validation(value) % Throws error if wrong
            pref.(prop).Value = value;
        end
        
        for iPr = 1:length(properties)
            prop = properties{iPr};
            if ~(isequal(prop,'DurationInSeconds')&&pref.IsContinuous.Value)
                set(session, prop, pref.(prop).Value)
            end
        end
        
        % Create Channels & Set Channel Properties
        for iChan = 1:size(channel_table.Data,1)
            chID = channel_table.RowName{iChan,:};
            enabled = channel_table.Data{iChan,1};
            range_str = channel_table.Data{iChan,2};
            range_num = range_table.num{ismember(range_table.view,range_str)};
            chName = channel_table.Data{iChan,3};
            if enabled
                ch = session.addAnalogInputChannel(device.ID, chID, 'Voltage');
                ch.Range = range_num;
                ch.Name = chName;
            end
        end
        pref.ChannelInfo_Input.Value = channel_table.Data;
        
        pref.OuterPosition.Value = max(0,fig.OuterPosition);
        
        % Save preferences to registry
        for iPr = 1:nProp
            propname = propnames{iPr};
            val = pref.(propname).Value;
            setpref(prefgp, propname, val);
        end
        
        result = struct();
        result.session = session;
        %% Test
        %%save ('Daq_data_init.mat' , 'session');
        %% Schlie�e Fenster
        close (fig)
        callback(result)
        
    end

end


function stct = prefentry(value,validation)
    stct = struct();
    stct.Value = value;
    stct.Validation = validation;
end



