% BioRecorder for recording with NI DAQ on Windows. 
% Data is saved to matfile immediately. 

%%
%% DO NOT MODIFY!
%%

addpath('BioProcessor_fixed')

if exist('br','var')
    delete(br);
end

clc
close all
clear classes
drawnow

dbstop if error

 br = BioRecorder();

% Emergency Stop:
% br.DAQ.session.stop();
                
                
                