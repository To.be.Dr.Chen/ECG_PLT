%   Participants of the BMT Lab at IBT have to modify 
%   these functions in exercise 7.


%% Get absolute blood presure from pulse transit time.
%% Parameter ptt contains the pulse transit time.
%% Return value mmHg contains the corresponsing blood pressure in mmHg.
%% This function should be able to process a vector of ptt values.
function mmHg = find_mmHg(ptt)

    % This is just a rough guess!
    % You should improve this by measuring your individual constants.
    mmHg =  -0.2*ptt*1000 + 176;
%      mmHg =  0;
    %% ADD YOUR CODE HERE
    
end