%   Participants of the BMT Lab at IBT have to modify
%   these functions in exercise 7.


%% Get heart frequency from ecg peaks.
%% Parameter all_peaks_ecg contains ECG peak times
%% Return value hf contains the heart frequency.
function hf = find_hf(all_peaks_ecg)
%Input
%           all_peaks_ecg : contains the detected peaks of the ECG
%           signal. Be careful with the sequence
%
%Output
%           hf : Vector containing heartrates, consider: you will need
%           2 heartrates to show tendency, in that case hf(1) is the
%           former heartrate, hf(2) the new one
%
hf=[-1,-1];
lgt=length(all_peaks_ecg);
if(lgt<2)
    hf=[-1 -1];
end
if (lgt<=8&&lgt>=2)
    temp1=all_peaks_ecg(end-1)-all_peaks_ecg(1);
    f1=60*(lgt-2)/temp1;
    temp2=all_peaks_ecg(end)-all_peaks_ecg(1);
    f2=60*(lgt-1)/temp2;
    hf(2)=f2;
    hf(1)=f1;
end
if(lgt==9)
    temp1=all_peaks_ecg(8)-all_peaks_ecg(1);
    f1=60*7/temp1;
    temp2=all_peaks_ecg(end)-all_peaks_ecg(end-8);
    f2=60*8/temp2;
    hf(2)=f2;
    hf(1)=f1;
end
if(lgt>9)
    temp2=all_peaks_ecg(end)-all_peaks_ecg(end-8);
    temp1=all_peaks_ecg(end-1)-all_peaks_ecg(end-9);
    f1=60*8/temp1;
    f2=60*8/temp2;
    hf(1)=f1;
    hf(2)=f2;
end
%% ADD YOUR CODE HERE


end