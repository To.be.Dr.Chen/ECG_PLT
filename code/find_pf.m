%   Participants of the BMT Lab at IBT have to modify
%   these functions in exercise 7.
%  Änderung Andreas Wachter 12.04.18 hinzugefügt
%%
% As for now this function is not used. It can be used though through some
% changes in the BioProcessor_read_data.m (line 79)
%% Get heart frequency from pleth peaks.
function pf = find_pf(all_peaks_pleth)
%Input
%           all_peaks_ecg : contains the detected peaks of the ECG
%           signal. Be careful with the sequence
%
%Output
%           pf : Vector containing heartrates, consider: you will need
%           2 heartrates to show tendency, in that case pf(1) is the
%           former heartrate, pf(2) the new one
%     persistent test
%     if isempty(test)
%         test = -1;
%     end

pf=[-1 -1];
lgt=length(all_peaks_pleth);
if(lgt<2)
    pf=[-1 -1];
end
if (lgt<=8&&lgt>=2)
    temp1=all_peaks_pleth(end-1)-all_peaks_pleth(1);
    f1=60*(lgt-2)/temp1;
    temp2=all_peaks_pleth(end)-all_peaks_pleth(1);
    f2=60*(lgt-1)/temp2;
    pf(2)=f2;
    pf(1)=f1;
end
if(lgt==9)
    temp1=all_peaks_pleth(8)-all_peaks_pleth(1);
    f1=60*7/temp1;
    temp2=all_peaks_pleth(end)-all_peaks_pleth(end-8);
    f2=60*8/temp2;
    pf(2)=f2;
    pf(1)=f1;
end
if(lgt>9)
    temp2=all_peaks_pleth(end)-all_peaks_pleth(end-8);
    temp1=all_peaks_pleth(end-1)-all_peaks_pleth(end-9);
    f1=60*8/temp1;
    f2=60*8/temp2;
    
    pf(1)=f1;
    
    pf(2)=f2;
    
end
%% ADD YOUR CODE HERE
end