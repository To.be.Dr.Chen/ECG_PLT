classdef (ConstructOnLoad) NewConnectionEventData < event.EventData
    properties
        % Sample rate in Hz, scalar double, p.ex. 1000
        SampleRate
        
        % Struct with information about channels: Name and Range
        Channels
    end
    
    methods
        function data = NewConnectionEventData(SampleRate, Channels)
            data.SampleRate = SampleRate;
            data.Channels = Channels;
        end
    end
end