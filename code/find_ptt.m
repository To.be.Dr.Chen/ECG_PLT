%   Implementation of pulse transit time calculations.
%   
%   Participants of the BMT Lab at IBT have to modify 
%   these functions in exercise 7.


%% Get pulse transit times
function [ptt_time,ptt_value] = find_ptt(all_peaks_ecg, all_peaks_plt)
    % Inputs
    %           all_peaks_ecg : contains all timestamps for detected peaks
    %           in the ECG signal.
    %           
    %           all_peaks_plt : contains all timestamps for detected peaks
    %           in the Pleth signal
    %
    %           !! careful first peak is always(!) all_peaks_...[1], new
    %           peaks are added to the end of the vector.
    %           Therefore it is useful to go backwards through the vector
    %           Feel free to flip it though
    %
    % Outputs
    %           ptt_time : vector containing the timestamps for the ptt
    %                      (e.g. go for the plethysmogram peak...)
    %           ptt_value: the related ptt values
    %
    %           Hint:    Always return several pairs (if possible), 10
    %           should be enough for the visualization, order is not
    %           important, keep in mind though that you have to return the
    %           recent ptt
    %
    %           
    ptt_time  = [];
    ptt_value = [];
    lgtecg=length(all_peaks_ecg);
    lgtplt=length(all_peaks_plt);
    if(lgtecg>=2&&lgtplt>=2)
    for i=2:lgtecg
        for j=1:lgtplt
            if((all_peaks_plt(j)>all_peaks_ecg(i-1))&&(all_peaks_plt(j)<all_peaks_ecg(i)))
                if((all_peaks_plt(j)-all_peaks_ecg(i-1))<0.4)
                ptt_time=[ptt_time all_peaks_plt(j)];
                ptt_value=[ptt_value all_peaks_plt(j)-all_peaks_ecg(i-1)+0.2];
                end
            end
        end
        
    end
    end

    %% ADD YOUR CODE HERE
    

end