classdef MyFilter < handle
    %MYFILTER IIR & FIR Realtime Filter 
    %   
    %   Custom implementation of realtime digital filter. 
    %   Can be used for ECG filtering. 
    %   
    %   Participants of the BMT Lab at IBT have to write 
    %   this class in exercise 5.
    %
    %   Pay attention with IIR filters of orders bigger than 3!
    %   Filter may become unstable due to rounding errors in [b,a] representation. 
    %   Use SOS Filtering for these cases instead. See:
    %   https://de.mathworks.com/help/signal/ref/butter.html#NumericalInstabilityFromBaSyntaxExample
    %
    
    
    properties
        
        % Coefficients - moving average
        b
        
        % Coefficients - recursion
        % may be 1 for FIR filters
        % always: a(1) = 1
        a
        
        % Filter State - input values
        % in(1) = current value
        % in(2) = last value 
        reg_in
        
        % Filter State - output values
        % out(1) = current value
        % out(2) = last value
        reg_out        
        
        % Approximate group delay for ECG in in samples
        delay_samples
        
    end
    
    
    methods
        %%sig_out = filter_single(mf,sig_in)
     
        function mf = MyFilter(b,a, delay_samples)
            % Constructor
            % b,a: Filter Coefficients
            % delay_samples: approximate delay for ECG signals. Is not used by MyFilter, 
            % but can be requested later for plotting purposes: mf. get_delay_samples()
            
            % Check input arguments
            check_arg(b)
            check_arg(a)
            assert(a(1)==1)
            
            % Change to column vectors and save
            mf.b = b(:);
            mf.a = a(:);
            
            % Find approximated delay for ECG signals
            if nargin>2
                mf.delay_samples = delay_samples;
            else
                mf.delay_samples = 0;
            end
            
            mf.reset();
        end
        
        function reset(mf)
            % Reset internal filter state
            mf.reg_in  = mf.b * 0;
            mf.reg_out = mf.a * 0;
        end
        
        function sig_out = filter(mf,sig_in)
            % Filters entire signal vector: signal is shifted through filter.
            % If the signal doesn't continue the previously filtered signal, 
            % you should reset the filter first. 
            
            % Call function for single value filtering repeatedly
            assert(isvector(sig_in))
            sig_out = NaN*sig_in;
            for ind = 1:length(sig_in)
                    [sig_out(ind)] = filter_single(mf, sig_in(ind));
            end
        end
        
        
        
        
        function delay = get_delay_samples(mf)
            delay = mf.delay_samples;
        end
        
    end
    
end


%% Local Helper Functions

function check_arg(b)
    % Input argument checking
    assert(isvector(b))
    assert(isa(b,'double'))
    assert(isreal(b))
    assert(~isempty(b))
end





