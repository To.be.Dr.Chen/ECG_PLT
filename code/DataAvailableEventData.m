classdef (ConstructOnLoad) DataAvailableEventData < event.EventData
    properties
        TimeStamps
        Data
    end
    
    methods
        function data = DataAvailableEventData(TimeStamps,Data)
            assert(size(TimeStamps,1)==size(Data,1))
            data.TimeStamps = TimeStamps;
            data.Data = Data;
        end
    end
end