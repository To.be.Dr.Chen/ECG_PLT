function [ b , a ] = design_filter_ecg( Samplerate )
%design_filter_ecg
%Part of Exercise 5
%Use this function to design you function to design your ECG Filter
%
%Feel free to use designfilt
%Inputs:
%Samplerate  	:       Integer containing the used Samplerate
%
%Outputs:
%b & a          :       Vectors containing the Filtercoefficients for
%                       FIR/IIR filtering. Consider a = [1] for an IIR-
%                       filter
%
%A good introduction to FIR/IIR filtering is found here:
%https://en.wikipedia.org/wiki/Finite_impulse_response
%https://en.wikipedia.org/wiki/Infinite_impulse_response
%% Design your filter here:

EKG_Hp=designfilt('highpassiir', 'FilterOrder', 4, 'HalfPowerFrequency', 0.5, 'SampleRate', 1000);
[b,a]=EKG_Hp.tf();
EKG_Nt=designfilt('bandstopiir', 'FilterOrder', 4, 'PassbandFrequency1', 45, 'PassbandFrequency2', 55, 'PassbandRipple', 1, 'SampleRate', 1000);
[y,x]=EKG_Nt.tf();
EKG_Nt=designfilt('bandstopiir', 'FilterOrder', 4, 'PassbandFrequency1', 95, 'PassbandFrequency2', 105, 'PassbandRipple', 1, 'SampleRate', 1000);
[m,n]=EKG_Nt.tf();
EKG_Tp=designfilt('lowpassfir', 'FilterOrder', 1500, 'PassbandFrequency', 70, 'StopbandFrequency', 75, 'SampleRate', 1000);
[t,p]=EKG_Tp.tf();
a = conv(a,x);
a = conv(a,n);
a = conv(a,p);

b = conv(b,y);
b = conv(b,m);
b = conv(b,t);

end

