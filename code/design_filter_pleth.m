 function [ b , a ] = design_filter_pleth( Samplerate )
%design_filter_pleth
%Part of Exercise 5
%Use this function to design you function to design your ECG Filter
%
%Feel free to use designfilt
%Inputs:
%Samplerate  	:       Integer containing the used Samplerate
%
%Outputs:
%b & a          :       Vectors containing the Filtercoefficients for
%                       FIR/IIR filtering. Consider a = [1] for an IIR-
%                       filter
%
%A good introduction to FIR/IIR filtering is found here:
%https://en.wikipedia.org/wiki/Finite_impulse_response
%https://en.wikipedia.org/wiki/Infinite_impulse_response
%% Design your filter here:
% b = [1,0,0];
% a = [1];
Ple_TF=designfilt('lowpassiir', 'FilterOrder', 10, 'HalfPowerFrequency', 25, 'SampleRate', 1000);
[b,a]=Ple_TF.tf();

Ple_Hp=designfilt('highpassfir', 'FilterOrder', 1000, 'StopbandFrequency', 0.3, 'PassbandFrequency', 0.5, 'SampleRate', 1000);
 [y,x]=Ple_Hp.tf();
 a = conv(a,x);
 b = conv(b,y);

end

