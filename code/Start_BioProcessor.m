    %% Start & Connect BioRecorder & BioProcessor

%%
%% DO NOT MODIFY!
%%

% If you start BioRecorder and BioProcessor with BioProcessor_Starter, 
% a new BioProcessor will be opened and connected to BioRecorder. 
% If you start a realtime acquisition then, the stream will be fed into the BioProcessor. 
% If you open a previously recorded file, you can playback the stream into the BioProcessor, too. 


%% Dependencies: 
% [flist,plist] = matlab.codetools.requiredFilesAndProducts('BioProcessor_Starter.m'); [flist'; {plist.Name}']
% 
% MATLAB; Signal Processing Toobox; Data Acquisition Toolbox
% BioProcessor.m; BioRecorder.m; DAQDialog.m; DataAvailableEventData.m; MyFilter.m; NLEO.m
% NewConnectionEventData.m; PeakDetector.m; plotECG.m; view_signal_statistics.m
%


%% Clear


% Delete previous instances 
if exist('br','var')
    delete(br);
end

% Clear workspace 
clc
close all hidden
clear classes
drawnow

dbstop if error



%% Create

% Create BioRecorder
br = BioRecorder();


% Create BioProcessor
bp = BioProcessor();


% Connect BioProcessor to BioRecorder
bp.connect_BioRecorder(br);

fprintf(' Connected  ')
fprintf('\n');
return
%% Emergency Stop
% (make sure to evaluate this in the base workspace where the object br/bp is visible)

br.DAQ.session.stop();
delete(bp.event_listener)

% In addition, you can type Ctrl-C in the Command Window