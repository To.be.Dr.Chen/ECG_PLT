function proc = BioProcessor_initialize(SampleRate)
% Initialize your signal processing objects and arrays here and put the handles into the proc struct. 
% It is stored in BioProcessor as a variable and you can access everything later. 
% This method is called by BioProcessor whenever the DAQ preferences like SampleRate have changed. 

% Put anything into proc you have to access repeatedly when new data packets arrive:
% - MyFilter realtime filter objects
% - Peak detector objects
% - Signal buffers (they should be implemented in your MyFilter and Peak detector classes though)
% - Buffers for previously detected peaks 


% Design Filters with current sample rate
fprintf('Designing Filters...    ')

proc = struct();

% Design ECG filters.

% Design ECG filters.
[b,a]=design_filter_ecg(SampleRate);

delay_samples = grpdelay(b,a, [10,10], SampleRate);
delay_samples = delay_samples(1);
proc.filter_ecg = MyFilter(b,a, delay_samples);




% Design Plt filters
[b,a]=design_filter_pleth( SampleRate );

delay_samples = grpdelay(b, a, [10,10], SampleRate);
delay_samples = delay_samples(1);
proc.filter_plt = MyFilter(b,a, delay_samples);


% Create and Parameterize Peak Detectors
proc.peakdetector_ecg   = ECGDetector();
proc.peakdetector_plt = PlethDetector();

%% YOU MAY SET YOUR DETECTOR PROPERTIES HERE



% Buffers
proc.all_peaks_ecg = [];
proc.all_peaks_plt = [];

fprintf('Done. \n')


end