function [sig_out] = filter_single(mf , sig_in)
            %Use this function to filter your Ecg/Pleth signal
            %
            % Inputs: 
            %           mf :    instance of the used Filter
            %           %
            %           mf.reg_in           
            %           mf.reg_out
            %           these 2 register are set to 0, you will have to update
            %           them by yourself
            %           mf.a / mf.b are your filtercoefficients
            %
            %           sig_in: double containing the value
            %
            %
            % Outputs:   sig_out : a single double containing the filtered
            %           value 
            %            
            %
            %Hint1 :    For FIR/IIR Filtering consider reading this: (if you have not done so already ;)
            %                                                              
            %           https://en.wikipedia.org/wiki/Infinite_impulse_response
            %
            %Hint2 :    Every Filter has 2 predefined registers reg_in and
            %           reg_out whose size has been adjusted to fit the
            %           length of your Filtercoefficients b & a
            %
            %Comment:   Additional properties can be added in MyFilter.m
            %           For some basic FIR/IIR Filterung you will not need
            %           to do this.
           
            
            % Check inputs
            assert(isscalar(sig_in))
            assert(isa(sig_in,'double'))
            assert(isreal(sig_in))
            sig_out = sig_in;
            %% ADD YOUR CODE HERE
            % Use the filter coefficients mf.a and mf.b to filter the next signal value sig_in and store the result in sig_out.
            % You need to use and update the shift registers mf.reg_in and mf.reg_out for this task.
            mf.reg_in = [sig_in; mf.reg_in(1:end-1)];
            sig_out = (1/mf.a(1))*(dot(mf.b,mf.reg_in)-dot(mf.a(2:end),mf.reg_out(1:end-1)));
            mf.reg_out = [sig_out; mf.reg_out(1:end-1)];
            %sig_out
        end
