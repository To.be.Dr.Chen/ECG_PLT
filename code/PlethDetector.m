classdef PlethDetector < PeakDetector
    %PLETHDETECTOR Plethysmogramm Peak Detector
    %   
    %   Realtime Plethysmogramm Peak Detector
    %   
    %   Participants of the BMT Lab at IBT have to modify 
    %   this class in exercise 6.
    %
    
    properties
        
        %% ADD OWN PROPERTIES HERE
        
    end
    
    
    methods        
        
        function peak = process_single(mf, time_in, sig_in)
            %% This is your primary function for the peakdetection
            %  
            %   Input: 
            %           mf    :     Instance of your Peakdetector (
            %           ECG/Pleth)
            %
            %           time_in, sig_in : contains a (filtered) signalvalue
            %           and its time
            %
            %
            %   Output:
            %           peak   :    time at which a peak occurs.
            %
            %           Consider:
            %           can be empty, you dont need to return a peak the
            %           moment it occurs...( Somewhere in your Guidelines
            %           there is a max delay mentioned though ;) )
            %           
            %
            %% Check inputs
            assert(isscalar(sig_in))
            assert(isa(sig_in,'double'))
            assert(isreal(sig_in))
            
            peak = [];
            
            %% ADD YOUR CODE HERE
            mf.Preg=[mf.Preg sig_in];
            mf.Treg=[mf.Treg time_in];
             
            lgx=length(mf.Preg);
            if(lgx==1380)
            temp=[];
            %[B,Lc]=findpeaks(mf.preg);
            Averg=mean(mf.Preg); %% average
            Abweic= std(mf.Preg); %%standard deviation
            Schw=Averg+Abweic; %%schwelle [Pks, Loc]=findpeaks(mf.Preg);
            %Schwslp=max(B)*2/3;
            [Pks, Loc]=findpeaks(mf.Preg, 'MinPeakDistance', 250);
%             [Pks, Loc]=findpeaks(mf.Preg);
             for idx=(1:length(Pks))
               if(Pks(idx)>Schw)
                   if ((length(temp))<1)
                     peak=[peak mf.Treg(Loc(idx))];
                     temp=[temp mf.Treg(Loc(idx))];
                   else
                       if((mf.Treg(Loc(idx))-temp(end))>0.5)
                           peak=[peak mf.Treg(Loc(idx))];
                           temp=[peak mf.Treg(Loc(idx))];
                       end
                   end
                   
               end
             end
            mf.Preg=[];
            mf.Treg=[];
           
             end
        end
    end
    
end