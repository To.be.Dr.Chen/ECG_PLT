classdef BioProcessor < handle
%BIOPROCESSOR Realtime Pulse Transit Time Visualization
%   BioRecorder cares for connection to the DAQ card. 
%   BioProcessor performs realtime filtering, 
%   marker detection, PTT calculation and visualization
%   on the recorded data. 
%
%   Participants of the BMT Lab at IBT have to write 
%   this class or parts of it on their own. 
%
%   If the axes label is not fully visible, resize the screen 
%   a little bit and it will be automatically adjusted. 
%
%
%   Version 1.2 - 27.02.2017 - Daniel Frisch 
%
%



%% Dependencies: 
% [flist,plist] = matlab.codetools.requiredFilesAndProducts('BioProcessor.m'); [flist'; {plist.Name}']
% 
% MATLAB; Signal Processing Toobox
% MyFilter.m, NLEO.m, PeakDetector.m
%



%% Properties
    
    properties
        
        % Struct with GUI-related variables
        % figure: figure handle
        % axes_ecg, axes_plt, axes_ptt: axes handles
        % ecg.line, plt.line, ptt.line: chart line handles
        % ecg.marker, plt.marker
        % menu.ctrl.main, menu.ctrl.stop, menu.ctrl.start, 
        % menu.ctrl.open, menu.ctrl.update, menu.ctrl.viewstat
        GUI = struct('figure',[], 'axes_ecg',[], 'axes_plt',[], 'axes_ptt',[], 'ecg',[], 'plt',[], 'ptt',[], 'info',[], 'menu',[]);
        
        % Visualization buffer length in seconds
        buffer_seconds = 5
        
        % Sample rate in Hz, scalar double, p.ex. 1000
        SampleRate
        
        % Struct with information about channels: Name and Range 
        channels
        
        % Indices of ecg and plt channel 
        ecg_ind, plt_ind 
        
        % Event Listener handle
        event_listener
        
        % Data Processing objects like MyFilter, NLEO, PeakDetector ... (you can define others/more)
        proc
        
        % Convenience
        delete_fcn
        
    end
    
    
    
    
    %% Methods
    
    methods
        
        function bp = BioProcessor()
            % BioRecorder Constructor
            
            bp0 = bp;
            % This nested function makes sure that BioProcessor 
            % can be closed even if it is not anymore on the Matlab Path 
            function close_request(varargin)
                delete(bp0.event_listener)
                delete(bp0.GUI.figure) % delete figure
            end
            bp.delete_fcn = @close_request;
            
            % Figure
            bp.GUI.figure = figure('CloseRequestFcn', @close_request);
            set(bp.GUI.figure, ...
                'Name','BioProcessor', ...
                'SizeChangedFcn',@bp.resizegui, ...
                'NumberTitle','off', ...
                'Renderer','Painters', ... % painters or opengl
                'GraphicsSmoothing','on', ...
                'HandleVisibility','callback',... % prevent commandline to plot into figure
                'Color','white');
            
            % Tool Menu
            bp.GUI.menu.tool.main = uimenu(bp.GUI.figure, 'Label','Tools');
            bp.GUI.menu.tool.prnCpBm1 = uimenu(bp.GUI.menu.tool.main, 'Label','Screenshot to Clipboard - 75 dpi', 'Callback',{@bp.screenshot_copyBm,'-r75'});
            bp.GUI.menu.tool.prnCpBm2 = uimenu(bp.GUI.menu.tool.main, 'Label','Screenshot to Clipboard - 150 dpi', 'Callback',{@bp.screenshot_copyBm,'-r150'});
            bp.GUI.menu.tool.prnCpBm3 = uimenu(bp.GUI.menu.tool.main, 'Label','Screenshot to Clipboard - 300 dpi', 'Callback',{@bp.screenshot_copyBm,'-r300'});
            
            % GUI Hierarchy
            bp.GUI.axes_ecg   = axes(bp.GUI.figure, 'NextPlot','add');
            bp.GUI.axes_plt   = axes(bp.GUI.figure, 'NextPlot','add');
            bp.GUI.axes_ptt   = axes(bp.GUI.figure, 'NextPlot','add');
            bp.GUI.ecg.line   = plot(bp.GUI.axes_ecg, 0,0);
            bp.GUI.plt.line   = plot(bp.GUI.axes_plt, 0,0);
            bp.GUI.ecg.marker = plot(bp.GUI.axes_ecg, 0,0);
            bp.GUI.plt.marker = plot(bp.GUI.axes_plt, 0,0);
            yyaxis(bp.GUI.axes_ptt,'right');
            bp.GUI.ptt.line   = plot(bp.GUI.axes_ptt, 0,0);
            bp.GUI.info = uicontrol(bp.GUI.figure, 'Style','edit', 'String',{''});
            
            % Link signal and PTT axes
            linkaxes([bp.GUI.axes_ecg, bp.GUI.axes_plt, bp.GUI.axes_ptt], 'x')
            
            % No points should be visible in the beginning
            set(bp.GUI.ecg.marker, 'XData',[], 'YData',[]);
            set(bp.GUI.plt.marker, 'XData',[], 'YData',[]);
            set(bp.GUI.ptt.line  , 'XData',[], 'YData',[]);
            
            % GUI Appearance. 
            % You can change values here and apply immediately by
            % executing the corresponding code lines, if you have 
            % the BioProcessor handle "bp" also in your current workspace.
            
            % ECG and plt axis
            bp.GUI.axes_ecg.Title.String = 'ECG ';
            bp.GUI.axes_plt.Title.String = 'Plethysmogram';
            bp.GUI.axes_ecg.ClippingStyle  = 'rectangle';
            bp.GUI.axes_plt.ClippingStyle  = 'rectangle';
            set(bp.GUI.axes_ecg, ...
                'Units','normalized',...
                'Position',[0.1,0.75,.7,.2], ...
                'XGrid','on', ...
                'YGrid','on', ...
                'XMinorGrid','on', ...
                'YMinorGrid','on');
            set(bp.GUI.axes_plt, ...
                'Units','normalized',...
                'Position',[0.1,0.55,.7,.2], ...
                'XGrid','on', ...
                'YGrid','on', ...
                'XMinorGrid','on', ...
                'YMinorGrid','on');
            bp.GUI.axes_ecg.YLabel.String = 'Voltage in mV';
            bp.GUI.axes_plt.YLabel.String = 'Voltage in mV';
            
            % PTT axis
            yyaxis(bp.GUI.axes_ptt,'left');
            bp.GUI.axes_ptt.Title.String = 'Pulse Transit Time and Blood Pressure Estimation';
            set(bp.GUI.axes_ptt, ...
                'Units','normalized',...
                'Position',[.1,.07,.7,.4], ...
                'XGrid','on', ...
                'YGrid','on', ...
                'XMinorGrid','on', ...
                'YMinorGrid','on', ...
                'YColor','black', ...
                'Box','on');
            bp.GUI.axes_ptt.YLabel.String = 'Estimated P_{sys} in mmHg';
            bp.GUI.axes_ptt.XLabel.String = 'Time in s';
            yyaxis(bp.GUI.axes_ptt,'right');
            set(bp.GUI.axes_ptt, ...
                'YDir','reverse',...
            	'YColor','black');
            bp.GUI.axes_ptt.YLabel.String = 'Pulse  Transit  Time in ms';
            yyaxis(bp.GUI.axes_ptt,'left');
            
            % ECG and plt Lines and Markers
            set(bp.GUI.ecg.line, ...
                'LineWidth',.5, ...
                'Color','black');
            
            set(bp.GUI.plt.line, ...
                'LineWidth',1, ...
                'Color',[ 0    0.4470    0.7410]);
            
            set(bp.GUI.ecg.marker, ...
                'LineStyle','none', ...
                'Color',[0.8500    0.3250    0.0980], ...
                'Marker','o');
            
            set(bp.GUI.plt.marker, ...
                'LineStyle','none', ...
                'Color',[0.9290    0.6940    0.1250], ...
                'Marker','o', ...
                'XData',[], ...
                'YData',[]);
            
            % PTT Line
            set(bp.GUI.ptt.line, ...
                'LineWidth',2, ...
                'Color','black', ...
                'Marker','o');
            
            % Info edit
            set(bp.GUI.info,...
                'FontName','FixedWidth',...
                'FontWeight','bold',...
                'Min',1, 'Max',3,... % multi-line
                'HorizontalAlignment','left',...
                'Position', [0.9,0,0.1,1], ...
                'Units','normalized');
            
            bp.resizegui()
        end
        
        
        function connect_BioRecorder(bp,bio_recorder)
            assert(isa(bio_recorder,'BioRecorder'))
            delete(bp.event_listener) % we can listen to only one BioRecorder
            l1 = bio_recorder.addlistener('DataAvailable', @bp.read_data);
            l2 = bio_recorder.addlistener('NewConnection', @bp.initialize);
            bp.event_listener = [l1,l2];
        end
        
        
        function initialize(bp,src,event)
            % Call this after the samplerate or number / name of channels could have changed. 
            % samplerate: scalar double, p.ex. 1000
            % channels  : cell array of strings with channel names, p.ex: {'ECG I', 'ECG II', 'Pleth'}
            
            % Process input arguments
            samplerate = event.SampleRate;
            chan = event.Channels;
            validateattributes(samplerate, {'double'}, {'real','finite','positive','scalar'});
            bp.SampleRate = samplerate;
            validateattributes(chan, {'struct'}, {})
            bp.channels = chan;
            
            fprintf('Initialize BioProcessor: Samplerate=%u; Channels=%s \n', bp.SampleRate, strjoin({bp.channels.Name},','))
            
            % Find out ECG and Pleth channel Index 
            if datenum(version('-date')) <= 736371 % 2016a or older
                warning('MATLAB version 2016a (or older) doesn''t provide the function ''contains()''. Using first channel as ECG and second channel as Pleth.')
                bp.ecg_ind = 1;
                bp.plt_ind = 2;
                if length(bp.channels)~=2
                    delete(bp.event_listener)
                    error('MATLAB 2016a or older: You must record with 2 channels; 1=ECG, 2=Pleth.')
                end
            else
                ind1 = contains({bp.channels.Name}, 'ECG', 'IgnoreCase',true);
                ind2 = contains({bp.channels.Name}, 'EKG', 'IgnoreCase',true);
                ind = ind1 | ind2;
                assert(any(ind), 'No channel name contains ''ECG''. Available channels: %s',strjoin({bp.channels.Name},', '))
                bp.ecg_ind = find(ind,1);
                fprintf('Using channel %u ''%s'', as ECG channel\n', bp.ecg_ind, bp.channels(bp.ecg_ind).Name);
                ind = contains({bp.channels.Name}, {'PLE','PLT'}, 'IgnoreCase',true);
                assert(any(ind), 'No channel name contains ''PLE'' or ''PLT''. Available channels: %s',strjoin({bp.channels.Name},', '))
                bp.plt_ind = find(ind,1);
            end
            fprintf('Using channel %u, ''%s'' as Plethysmogram channel\n', bp.plt_ind, bp.channels(bp.plt_ind).Name);
                        
            % Reset Visualization Buffers
            vec = NaN(bp.SampleRate*bp.buffer_seconds,1);
            bp.GUI.ecg.line.XData = vec;
            bp.GUI.ecg.line.YData = vec;
            bp.GUI.plt.line.XData = vec;
            bp.GUI.plt.line.YData = vec;
            bp.GUI.ecg.marker.XData = [];
            bp.GUI.ecg.marker.YData = [];
            bp.GUI.plt.marker.XData = [];
            bp.GUI.plt.marker.YData = [];
            bp.GUI.ptt.line.XData = [];
            bp.GUI.ptt.line.YData = [];
            
            % Call custom initialize function
            bp.proc = BioProcessor_initialize(bp.SampleRate);
            
            validateattributes(bp.proc, {'struct'}, {'scalar'})
            
            bp.resizegui()           
            drawnow
            
            % bring BioProcessor to front
            figure(bp.GUI.figure) 
        end
        
        
        function read_data(bp,src,event)
            % This function is calles once a chunk of samples arrived
            %fprintf('read_data, %u samples\n',length(event.TimeStamps))
            
            time = event.TimeStamps; % [100 x 1] double
            dat  = event.Data;       % [100 x n] double
            
            dat_ecg = dat(:,bp.ecg_ind);
            dat_plt = dat(:,bp.plt_ind);
            
            % Call custom-editable function
            BioProcessor_read_data(bp,time,dat_ecg,dat_plt)
            
            bp.update_limits();
            drawnow 
            
            % Resize GUI after 2s automatically
            if time(1)<=2 && time(end)>=2
                bp.resizegui();
            end
        end
        
        
        function visualize_line(bp, gui_struct, time, data)
            % Appends data to line. 
            % line_handle: either "bp.GUI.ecg.line" or "bp.GUI.plt.line"
            gui_struct.line.XData = BioProcessor.shift_vector(gui_struct.line.XData, time);
            gui_struct.line.YData = BioProcessor.shift_vector(gui_struct.line.YData, data);
        end
        
        
        function visualize_markers(bp, gui_struct, times) 
            % Appends data to marker series 
            % marker_handle: either "bp.GUI.ecg.marker" or "bp.GUI.plt.marker" 
            gui_struct.marker.XData = times;
            % find according y values
            [~,ind] = min(abs(bsxfun(@minus,gui_struct.line.XData',times)),[],1);
            data = gui_struct.line.YData(ind);
            gui_struct.marker.YData = data; 
        end 
        
        
        function visualize_ptt(bp, ptt_time, ptt_value)
            bp.GUI.ptt.line.XData = ptt_time;
            bp.GUI.ptt.line.YData = ptt_value*1000;
        end
        
        
        function update_limits(bp)
            % Sets axes limits according to data
            tm = bp.GUI.ecg.line.XData';
            if ~all(isnan(tm(:,1)))
                % ECG and Plt axis
                endtime = tm(end);
                starttime = endtime - diff(tm(end-1:end))*size(tm,1);
                bp.GUI.axes_ecg.XLim = [starttime, endtime];
                bp.GUI.axes_ecg.YLim = BioProcessor.data_to_limits(bp.GUI.ecg.line.YData);
                bp.GUI.axes_plt.YLim = BioProcessor.data_to_limits(bp.GUI.plt.line.YData);
                
                % PTT axis
                yyaxis(bp.GUI.axes_ptt,'right');
                ptt_lim = bp.GUI.axes_ptt.YLim/1000;
                bp_lim  = find_mmHg(ptt_lim);
                yyaxis(bp.GUI.axes_ptt,'left');
                bp.GUI.axes_ptt.YLim = flip(bp_lim);
            end
        end        
        
            
        
        function screenshot_copyBm(bp,src,event,res)
            function enabler(bp)
                set(bp.GUI.figure, 'Pointer','arrow')
            end
            cleaner = onCleanup(@() enabler(bp));
            bp.GUI.figure.Pointer = 'watch';
            print(bp.GUI.figure, '-clipboard', '-dbitmap', res)
        end
        
        
        function resizegui(bp,varargin)
            % GUI layout
            
            % part of window for PTT axis
            ptt_share = 0.5;
            
            units = 'centimeters';
            bp.GUI.figure.Units = units;
            
            %space = 0.05;
            space = 0;
            info_w = 3;
            width = bp.GUI.figure.Position(3)-info_w;
            height = bp.GUI.figure.Position(4);
            
            % PTT axis
            yPos = space;
            ax_height = height * ptt_share;
            ax = bp.GUI.axes_ptt;
            if ~isempty(ax) && isvalid(ax)
                ax.Units = units;
                insets = ax.TightInset;
                pos = [space, yPos, width-2*space, ax_height-2*space];
                yPos = pos(2)+pos(4)+space;
                pos = pos + [insets(1), insets(2), -insets(1)-insets(3), -insets(2)-insets(4)];
                pos = max(0,pos);
                ax.Position = pos;
            end
            
            % Plt axis
            ax_height = height * (1-ptt_share)*.5;
            ax = bp.GUI.axes_plt;
            if ~isempty(ax) && isvalid(ax)
                ax.Units = units;
                insets = ax.TightInset;
                pos = [space, yPos, width-2*space, ax_height-2*space];
                yPos = pos(2)+pos(4)+space;
                pos = pos + [insets(1), insets(2), -insets(1)-insets(3), -insets(2)-insets(4)];
                pos = max(0,pos);
                ax.Position = pos;
            end
            
            % ECG axis
            ax_height = height * (1-ptt_share)*.5;
            ax = bp.GUI.axes_ecg;
            if ~isempty(ax) && isvalid(ax)
                ax.Units = units;
                insets = ax.TightInset;
                pos = [space, yPos, width-2*space, ax_height-2*space];
                yPos = pos(2)+pos(4)+space;
                pos = pos + [insets(1), insets(2), -insets(1)-insets(3), -insets(2)-insets(4)];
                pos = max(0,pos);
                ax.Position = pos;
            end
            
            % Info edit
            bp.GUI.info.Units = units;
            pos = [bp.GUI.figure.Position(3)-info_w, 0, info_w, bp.GUI.figure.Position(4)];
            pos = max(0,pos);
            bp.GUI.info.Position = pos;
        end
            
        
        function delete(bp)
            % Destructor
            bp.delete_fcn();
        end
      
    end
    
    
    
    
    
    %% Static Helper Functions 
    methods (Static)
        
        function c = shift_vector(a,b)
            % a: Original vector. Output vector will have the same length as a. 
            % b: Vector to be appended at the end. 
            % c: Output vector, cropped to same length as a. Will be column vector. 
            assert(isvector(a))
            assert(isvector(b))
            c = [a(:); b(:)];
            c = c(end-length(a)+1:end);
        end
        
        function [all_b,all_a] = conv_filters(varargin)
            % Concatenates Filter Coefficients. 
            % Works well for FIR filters. 
            % The IIR order should be below 4 for this!
            all_b = 1;
            all_a = 1;
            args = varargin;
            for ind = 1:nargin
                filt = args{ind};
                [b,a] = filt.tf;
                all_b = conv(all_b,b); 
                all_a = conv(all_a,a);
            end
        end
        
        function lm = data_to_limits(dat)
            mn = min(dat);
            mx = max(dat);
            delta = (mx-mn)*0.1;
            lm = [mn-delta,mx+delta];
        end
        
        
    end
    
    
    
end

