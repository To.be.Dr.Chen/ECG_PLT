function view_signal_statistics(bp)
% Opens a figure with several tabs that show the signal,
% PTT, RR, PP as function of time and their spectra,
% and the signal's spectrum.


if isempty(bp.data) || all(isnan(bp.data(:)))
    warning('No data available.')
    return;
end

ind = isfinite(bp.data(:,1));
data = bp.data(ind,:);




%% Initialization

% Figure
figS = figure();
set(figS, ...
    'Name','Signal Statistics', ...
    'NumberTitle','off', ...
    'Renderer','Painters', ... painters or opengl
    'GraphicsSmoothing','on', ...
    'DefaultUitabBackgroundColor','white', ...
    'Color','white');
% 'DefaultAxesNextPlot','add', ... default 'hold on'

% Parent of tabs
tabgp = uitabgroup(figS, ...
    'TabLocation','left', ...
    'Units','normalized', ...
    'Position',[0,0,1,1]);

% Datacursor
dcm_obj = datacursormode(figS);
set(dcm_obj,...
    'DisplayStyle','datatip', ...
    'Enable','off', ...
    'SnapToDataVertex','on',...
    'UpdateFcn',@datacursor);





%% ECG and Pleth Welch

tab = uitab(tabgp, 'Title','ECG Pleth Welch');
ax = axes(tab);
ax.Title.String = 'ECG & Pleth Power Spectrum Density';
cla

[ECG_freq, ECG_welch] = pwelchMed(data(:,2), bp.sample_rate,0.1);
[PLE_freq, PLE_welch] = pwelchMed(data(:,3), bp.sample_rate,0.1);
ECG_welch = ECG_welch / max(ECG_welch);
PLE_welch = PLE_welch / max(PLE_welch);
semilogx(ECG_freq,ECG_welch, 'LineWidth',2, 'Color','black'); hold on;
semilogx(PLE_freq,PLE_welch, 'LineWidth',2, 'Color',[ 0    0.4470    0.7410])
%semilogx(ECG_freq,20*log10(ECG_welch), PLE_freq,20*log10(PLE_welch))
legend({'ECG','Pleth'})
xlabel('Frequency in Hz')
ylabel('Normalized Power Spectrum Density Estimate')
grid on
grid minor
xlim([0.5,100])
drawnow





%% Further tabs go here

tab = uitab(tabgp, 'Title','New');
ax = axes(tab);
ax.Title.String = 'Further tabs go here';




end












%% Helper Functions for Welch Spectrum Estimation


function [freq, pxx] = pwelchMed_interp(time,ampl, fs,f_res)
% Helper function for Welch estimation of
% non-uniformly sampled data.
% First an interpolation to frequency fs is done.
% RR, PTT, PlethPeaks: fs=10, f_res=0.007

if isempty(time)
    freq = [];
    pxx = [];
    return;
end

% Interpolation to equi-distant samples
time2 = min(time):1/fs:max(time);
ampl2 = interp1(time,ampl,time2,'spline');

% Estimation with uniformly sampled data
[freq,pxx] = pwelchMed(ampl2, fs,f_res);
end




function [freq, pxx] = pwelchMed(ampl, fs, f_res)
% Helper function for Welch PSD estimation of
% uniformly sampled data (frequency fs).

% Remove mean (Peak at f=0)
%ampl = ampl-mean(ampl);
ampl = detrend(ampl);

% Window definition
winL = round(fs/f_res);
% if signal is too short, we must live with noise
winL = min(length(ampl),winL);
%fprintf('%f\n',length(ampl)/winL)
noverlap = round(winL*0.7);
win = blackman(winL);
nfft = max(256,2^nextpow2(winL))*4;

% Power Spectrum Density Estimation
[pxx,freq] = pwelch(ampl,win,noverlap,nfft,fs,'power');
end






