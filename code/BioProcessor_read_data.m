function BioProcessor_read_data(bp, time, dat_ecg, dat_plt)
% Update function that is called by BioProcessor whenever a new data block is available, p.ex. 100 samples. 
% The size of the data blocks can be adjusted in BioRecorder, "DAQ CONTROL"->"Configure DAQ". 
% Before the recording starts, all the filters must be initialized in BioProcessor_initialize.m. 
% The struct this function returned is accessible from here via bp.proc.
%
% Input Arguments:
%   - bp: BioProcessor handle
%   - time: vector of new timesteps
%   - dat_ecg: vector of new ECG values (corresponding to time)
%   - dat_plt: vector of new plethysmogram values (corresponding to time)
%
% You can filter the raw ECG and plethysmogram values using your instances of MyFilter. 
% Decide whether you want to visualize the original or filtered ECG and plethysmogram:
%   >> bp.visualize_line(bp.GUI.ecg, time, dat_ecg) % ECG visualization
%   >> bp.visualize_line(bp.GUI.plt, time, dat_plt) % pleth visualzation
%
% Then, detect the peaks or other markers in your signal and store the results for later access. 
% Tell BioProcessor bp to visualize your markers: 
%   >> bp.visualize_line(bp.GUI.ecg, time, dat_ecg)
%   >> bp.visualize_markers(bp.GUI.plt, bp.proc.all_peaks_plt - delay_plt)
%
% Now you are ready to calculate the Pulse Transit Time. 
% Visualize your results with the BioProcessor:
%   >> bp.visualize_ptt(ptt_time,ptt_value);
%
% Finally create a string that lists the current heart rate and puls transit time
% as well as information whether it is increasing or decreasing. 
% Use a cell array of strings to display multiple lines. 
%   >> str = {'Heart Rate','70'}; 
%   >> bp.GUI.info.String = str;
%


%% Signal Processing

% Realtime ECG Filtering
dat_ecg_filt = bp.proc.filter_ecg.filter(dat_ecg);
delay_ecg = bp.proc.filter_ecg.get_delay_samples()/bp.SampleRate;

% Realtime PLT Filtering
dat_plt_filt = bp.proc.filter_plt.filter(dat_plt);
delay_plt = bp.proc.filter_plt.get_delay_samples()/bp.SampleRate;

% Plot the signals you want on the ECG and plt channel: original or filtered 
bp.visualize_line(bp.GUI.ecg, time, dat_ecg_filt)
bp.visualize_line(bp.GUI.plt, time, dat_plt_filt)


%% Peak Detection

% Find Peaks
peaks_ecg = bp.proc.peakdetector_ecg.process(time, dat_ecg_filt);
peaks_plt = bp.proc.peakdetector_plt.process(time, dat_plt_filt);

% Remember Peaks
bp.proc.all_peaks_ecg = [bp.proc.all_peaks_ecg, peaks_ecg];
bp.proc.all_peaks_plt = [bp.proc.all_peaks_plt, peaks_plt];

% Plot detected markers
if(~isempty(bp.proc.all_peaks_ecg))
    bp.visualize_markers(bp.GUI.ecg, bp.proc.all_peaks_ecg);%%- delay_ecg
end
if(~isempty(bp.proc.all_peaks_plt))
    bp.visualize_markers(bp.GUI.plt, bp.proc.all_peaks_plt );%%- delay_plt
end


%% PTT

% Get Pulse Transit Time from Markers
[ptt_time,ptt_value] = find_ptt(bp.proc.all_peaks_ecg, bp.proc.all_peaks_plt);
bp.visualize_ptt(ptt_time,ptt_value);

%% Text

% Update Text
[hrt_ud,hrt_act] = up_or_down(find_hf(bp.proc.all_peaks_ecg));
[prt_ud,prt_act] = up_or_down(find_pf(bp.proc.all_peaks_plt));
[ptt_ud,ptt_act] = up_or_down(ptt_value*1000);

str = {
    '----------'
    ''
    ''
    'HR'
    sprintf('%.1f bpm',hrt_act)
    hrt_ud
    ''
    ''
    '----------'
    ''
    ''
    'PR'
    sprintf('%.1f ms',prt_act)
    prt_ud
    ''
    '----------'
    ''
    ''
    ''
    ''
    ''
    ''
    ''
    'PTT'
    sprintf('%.1f ms',ptt_act)
    ptt_ud
    ''
    '----------'
    };
bp.GUI.info.String = str;


end


%% Helper Functions
% You can define any functions you want to call from BioProcessor_read_data here. 
% These functions are only accessible from this .m file though. 


function [str,act,last] = up_or_down(values)
if length(values)>=2
    last = values(end-1);
    act = values(end);
    if abs(act-last)<0.1
        str = '(equal)';
    elseif act > last
        str = '(up)';
    else
        str = '(down)';
    end
else
%    str = latex(Uparrow);
    str = '(wait...)';
    last = 0;
    act = 0;
end
end