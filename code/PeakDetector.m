 
classdef (Abstract) PeakDetector < handle
    %PEAKDETECTOR Peak Detector
    %   
    %   Realtime Peak Detector Superclass.
    %   
    %   Participants of the BMT Lab at IBT have to modify 
    %   this class in exercise 6.
    %
    
    properties
        
        % Current Thresholds
        thresh
        Preg
        Treg
    
        %% YOU CAN ADD OWN PROPERTIES HERE
        %% Don't forget to initialize them in the constructor!
        
    end
    
    
    methods
        
        function mf = PeakDetector
            % Constructor
            mf.reset()
        end
        
        function reset(mf)
            mf.thresh = 0;
            mf.Preg=[];
            mf.Treg=[];
          
        end
        
        
        function [peaks,detailed] = process(mf, time_in, sig_in)
            % Filters entire signal vector
            % peaks: vector with timestamps a peak has been detected
            % detailed: struct with additional information, like current threshold
            
            % Simply call processing function for single value filtering repeatedly
            check_arg(time_in)
            check_arg(sig_in)
            assert(length(sig_in)==length(time_in))
            
            nSamples = length(sig_in);
            
            detailed = struct();
            detailed.threshold = nan(nSamples,1);
            peaks = [];
            for ind = 1:nSamples
                detailed.threshold(ind) = mf.thresh;
                pk = mf.process_single(time_in(ind),sig_in(ind));
                if ~isempty(pk)
                    peaks = [peaks,pk];
                end
            end
        end        
        
    end
    
    methods (Abstract)
        
        % Process single signal value
        % If a peak is detected, its timestamp is returned. 
        % (detected peak could be in the past)
        % Otherwise, the output argument is empty. 
        process_single(mf, time_in, sig_in)
        
    end
    
end


% Local helper function

function check_arg(b)
    % Input argument checking
    assert(isvector(b))
    assert(isa(b,'double'))
    assert(isreal(b))
    assert(~isempty(b))
end